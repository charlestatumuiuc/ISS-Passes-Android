
Coding Challenge by Charles Tatum II
Recruiter Contact: Lisa Yensen, The Judge Group

The zipped file on GitLab contains the source code for the Android portion of the International Space Station Passes Coding Challenge.

Due to the 10 MB limit on uploads, the full package was not uploaded. A subset of the Android Studio 3.0 files has been uploaded here to GitLab.

A *full* package - containing all Gradle files, binaries, etc. - can be downloaded from a OneDrive folder using this shared URL:

************************************************
https://1drv.ms/f/s!AkVGsVBrSJnRkFk_oKqDR4z3GZc1
************************************************

ISS-004-Android-final.zip - Contains all Java and Gradle files, created using Android Studio 3.0.1
ISS-004-Android-screen-shots.zip - Contains screen shots.

